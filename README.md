# Michael Black Ritter

## Developments in 2024

  - Monorepo-DevOps (Demo on [GitLab][mbr-demo@lab] and [GitHub][mbr-demo@hub])

  - Nuxt/Vue and Angular with Ionic and Capacitor ([GitLab][mbr-nuxonic@lab] and [GitHub][mbr-nuxonic@hub])

  - [Changesets with integration of Conventional Commits][changeset-conventional-commits@pr29]


## Focuses for 2024

  - [Effect-TS][effect-ts]

  - [SurrealDB][surrealdb] (Esp. [SurrealDB-WASM][surrealdb-wasm])


<!-- ![Black](assets/black.png) -->

<div align="center">
    <img alt="Black" src="https://gitlab.com/mblackrittr/mblackrittr/-/raw/master/assets/black.png" width="100%">
</div>


*Last updated (2024-05-05 05:47)*


<!-- urls -->
[changeset-conventional-commits@pr29]: https://github.com/iamchathu/changeset-conventional-commits/pull/29
[effect-ts]: https://www.effect.website/
[effect-ts@git]: https://github.com/effect-ts/effect-ts/
[mbr-demo@lab]: https://gitlab.com/mblackrittr/mbr-demo/
[mbr-demo@hub]: https://github.com/mblackrittr/mbr-demo/
[mbr-nuxonic@lab]: https://gitlab.com/mblackrittr/mbr-demo/tree/master/apps/nuxt-ionic/
[mbr-nuxonic@hub]: https://github.com/mblackrittr/mbr-demo/tree/master/apps/nuxt-ionic/
[surrealdb]: https://surrealdb.com/
[surrealdb-wasm]: https://github.com/surrealdb/surrealdb.wasm/
